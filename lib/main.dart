import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:widget_of_the_week/expanded_page.dart';
import 'package:widget_of_the_week/fade_transition_page.dart';
import 'package:widget_of_the_week/fadein_image_page.dart';
import 'package:widget_of_the_week/floating_action_button_page.dart';
import 'package:widget_of_the_week/future_builder_page.dart';
import 'package:widget_of_the_week/inherited_model_page.dart';
import 'package:widget_of_the_week/opacity_page.dart';
import 'package:widget_of_the_week/page_view_page.dart';
import 'package:widget_of_the_week/safe_area_page.dart';
import 'package:widget_of_the_week/sliver_app_bar_page.dart';
import 'package:widget_of_the_week/sliver_list_page.dart';
import 'package:widget_of_the_week/stream_builder_page.dart';
import 'package:widget_of_the_week/table_page.dart';
import 'package:widget_of_the_week/wrap_page.dart';

import 'animated_container_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Widget of the Week'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: _buildDemoListView());
  }

  ///將所有的示範項目顯示在一個清單上
  Widget _buildDemoListView() {
    List<HashMap> mList = [];
    HashMap hashMap = new HashMap();
    hashMap['title'] = 'Safe Area';
    hashMap['subTitle'] = '使內容可以在特殊形狀的螢幕正常顯示';
    hashMap['route'] = SafeAreaPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'Expanded';
    hashMap['subTitle'] = '使特定元件占更多空間';
    hashMap['route'] = ExpandedPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'Wrap';
    hashMap['subTitle'] = '動態分配項目的位置，空間不夠的話就會放到下一行。';
    hashMap['route'] = WrapPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'AnimatedContainer';
    hashMap['subTitle'] = '有動畫功能的容器，參數改變時，元件會以動畫的形式產生變化。';
    hashMap['route'] = AnimatedContainerPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'Opacity';
    hashMap['subTitle'] = '調整元件的透明度';
    hashMap['route'] = OpacityPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'FutureBuilder';
    hashMap['subTitle'] = '非同步載入帶資料的元件';
    hashMap['route'] = FutureBuilderPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'FadeTransition';
    hashMap['subTitle'] = '淡入／淡出元件';
    hashMap['route'] = FadeTransitionPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'FloatingButtonAction';
    hashMap['subTitle'] = '出現在畫面右下角的圓形按鈕';
    hashMap['route'] = FloatingActionButtonPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'PageView';
    hashMap['subTitle'] = '在一個畫面裡建立分頁';
    hashMap['route'] = PageViewPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'Table';
    hashMap['subTitle'] = '用表格編排元件';
    hashMap['route'] = TablePage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'SliverAppBar';
    hashMap['subTitle'] = '建立可配合畫面滾動伸展、隱藏的 App Bar。';
    hashMap['route'] = SliverAppBarPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'SliverList & SliverGrid';
    hashMap['subTitle'] = '進階清單實作';
    hashMap['route'] = SliverListPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'FadeInImage';
    hashMap['subTitle'] = '從網路載入圖片';
    hashMap['route'] = FadeInImagePage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'StreamBuilder';
    hashMap['subTitle'] = '非同步處理數據';
    hashMap['route'] = StreamBuilderPage();
    mList.add(hashMap);

    hashMap = new HashMap();
    hashMap['title'] = 'InheritedModel';
    hashMap['subTitle'] = '便於在巢狀布局中傳遞數據';
    hashMap['route'] = InheritedModelPage();
    mList.add(hashMap);

    return ListView.separated(
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(
              mList[index]['title'],
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              mList[index]['subTitle'],
              style: TextStyle(fontSize: 12),
            ),
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (BuildContext context) {
                return mList[index]['route'];
              }));
            },
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return Divider(color: Colors.blue, height: 0.0);
        },
        itemCount: mList.length);
  }
}
