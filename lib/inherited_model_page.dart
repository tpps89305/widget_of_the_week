import 'package:flutter/material.dart';

class InheritedModelPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => InheritedModelPageState();
  
}

class InheritedModelPageState extends State<InheritedModelPage> {
  int num1 = 0;
  int num2 = 0;

  void incNum1() {
    setState(() {
      num1++;
    });
  }

  void incNum2() {
    setState(() {
      num2++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Inherited Model')),
      body: Inherited(
        num1: num1,
        num2: num2,
        parent: this,
        child: const Child()
      )
    );
  }
}

class Child extends StatelessWidget {
  const Child();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Counter1(),
        Counter2()
      ],
    );
  }
}

class Inherited extends InheritedModel<String> {
  final int num1;
  final int num2;
  final InheritedModelPageState parent;

  static Inherited? of(BuildContext context, String aspect) {
      return InheritedModel.inheritFrom<Inherited>(context, aspect: aspect);
  }

  Inherited({
    Key? key,
    required Widget child,
    required this.num1,
    required this.num2,
    required this.parent
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(Inherited oldWidget) {
    return num1 != oldWidget.num1 || num2 != oldWidget.num2;
  }

  @override
  bool updateShouldNotifyDependent(Inherited oldWidget, Set<String> dependencies) {
    var updateA = num1 != oldWidget.num1 && dependencies.contains('num1');
    var updateB = num2 != oldWidget.num2 && dependencies.contains('num2');
    return updateA || updateB;
  }
}

class Counter1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Inherited c = Inherited.of(context, 'num1')!;
    return ElevatedButton(
      child: Text('${c.num1}'),
      onPressed: c.parent.incNum1
    );
  }
}

class Counter2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Inherited c = Inherited.of(context, 'num2')!;
    return ElevatedButton(
      child: Text('${c.num2}'),
      onPressed: c.parent.incNum2
    );
  }
}