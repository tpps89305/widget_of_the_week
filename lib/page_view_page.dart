import 'package:flutter/material.dart';

class PageViewPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PageViewPageState();
}

class PageViewPageState extends State<PageViewPage> {
  // 建立分頁控制器，一開始顯示第 2 個分頁。
  // 在 initialPage 參數內填入 1 是因為陣列是從 0 開始數。
  final _controller = PageController(initialPage: 1);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(title: Text('PageView')),
      body: PageView(
        controller: _controller,
        children: <Widget>[
          _buildFoodPage(),
          _buildSwimmingPage(),
          _buildHistoryPage()
        ],
      ),
    );
  }

  //建立「食物」分頁
  Widget _buildFoodPage() {
    return Align(
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Image(image: AssetImage('res/food.jpg')),
          Text('飲食', style: TextStyle(fontSize: 22)),
          Text('Photo by Frame Harirak on Unsplash', style: TextStyle(color: Colors.grey),)
        ],
      ),
    );
  }

  //建立「游泳」分頁
  Widget _buildSwimmingPage() {
    return Align(
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Image(image: AssetImage('res/swimming.jpg')),
          Text('游泳', style: TextStyle(fontSize: 22)),
          Text('Photo by sergio souza on Unsplash', style: TextStyle(color: Colors.grey),)
        ],
      ),
    );
  }

  //建立「歷史」分頁
  Widget _buildHistoryPage() {
    return Align(
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Image(image: AssetImage('res/history.jpg')),
          Text('歷史', style: TextStyle(fontSize: 22)),
          Text('Photo by Austrian National Library on Unsplash', style: TextStyle(color: Colors.grey),)
        ],
      ),
    );
  }
}
