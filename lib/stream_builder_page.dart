import 'package:flutter/material.dart';

class StreamBuilderPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StreamBuilderPageState();
  
}

class StreamBuilderPageState extends State<StreamBuilderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Stream Builder')),
      body: StreamBuilder(
        stream: counter(),
        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
          if (snapshot.hasError) {
            return Text('連線時發生錯誤');
          }
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text('沒有 Stream');
            case ConnectionState.waiting:
              return Text('等候 Stream');
            case ConnectionState.active:
              return Text('串流進行中：${snapshot.data}');
            case ConnectionState.done:
              return Text('串流完畢');
          }
        },
      ),
    );
  }

  Stream<int> counter() {
    return Stream.periodic(Duration(seconds: 1), (i) {
      return i;
    });
  }
  
}