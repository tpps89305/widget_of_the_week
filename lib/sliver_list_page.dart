import 'dart:math' as math;

import 'package:flutter/material.dart';

class SliverListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SliverListPageState();
}

class SliverListPageState extends State<SliverListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Sliver List')),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
                return _listItem(getRandomColor(), "第 $index 個項目");
              }, childCount: 10),
            ),
            SliverGrid(
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
                return _listItem(getRandomColor(), "第 $index 個項目");
              }, childCount: 9),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3
              ),
            )
          ],
        ));
  }

  Widget _listItem(Color color, String title) => Container(
        height: 50,
        color: color,
        child: Center(
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      );
}

getRandomColor() => Color((math.Random().nextDouble() * 0xFFFFFF).toInt() << 0)
    .withOpacity(1.0);
