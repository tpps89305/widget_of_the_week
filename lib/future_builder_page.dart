import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';

class FutureBuilderPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FutureBuilderPageState();
}

class FutureBuilderPageState extends State<FutureBuilderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(title: Text('Future Builder')),
        body: FutureBuilder(
          future: _getDataFromNetwork(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasData) {
                // 成功取得資料，依需求顯示資訊。
                // 在這個案例中， FutureBuilder 的 future 參數代入的方法會回傳 String，
                // 所以 snapshot.data 是 String。
                return SingleChildScrollView(
                  child: Text(snapshot.data.toString())
                );
              } else {
                // 讀取資料時發生錯誤，在畫面中央顯示紅色圓框叉叉。
                return Align(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        size: 48,
                        color: Colors.redAccent,
                      ),
                      Text(
                        '讀取時發生錯誤！',
                        style: TextStyle(color: Colors.redAccent),
                      )
                    ],
                  ),
                );
              }
            } else {
              // 等候資料下載，在畫面中央顯示灰色圖案。
              return Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(
                      Icons.autorenew,
                      size: 48,
                      color: Colors.grey,
                    ),
                    Text(
                      '正在下載資料',
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
              );
            }
          },
        ));
  }

  ///下載《Dcard》熱門消息（原始資料）
  ///
  ///回傳的資料型別是 [String]
  Future _getDataFromNetwork() async {
    return new Future.delayed(new Duration(seconds: 1), () async {
      var httpClient = new HttpClient();
      Map<String, dynamic> params = new Map<String, dynamic>();
      params["limit"] = "10";
      params["popular"] = "true";
      var url = new Uri.https("www.dcard.tw", "/service/api/v2/posts", params);
      var request = await httpClient.getUrl(url);
      var response = await request.close();
      var responseBody = await response.transform(Utf8Decoder()).join();
      return responseBody;
    });
  }
}
