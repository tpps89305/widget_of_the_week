import 'package:flutter/material.dart';

class WrapPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => WrapPageState();
}

class WrapPageState extends State<WrapPage> {
  List<String> titles = [
    '單人',
    '角色扮演',
    '橫向式動作',
    '動漫改編',
    '末日求生',
    '獵奇',
    '未滿 10 歲請勿購買',
    '霹靂卡霹靂拉拉 波波力那貝貝魯多'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text('Wrap'),
      ),
      body: Container(
        padding: EdgeInsets.all(8),
        child: Wrap(
          children: _buildTags(titles),
          spacing: 8,
          runSpacing: 8,
        ),
      ),
    );
  }

  /// 從陣列建立所有項目
  List<Widget> _buildTags(List<String> titles) {
    List<Widget> tags = [];
    for (String title in titles) {
      Widget tag = ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
          color: Colors.greenAccent,
          child: Text(
            title,
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
          padding: EdgeInsets.all(8),
        ),
      );
      tags.add(tag);
    }
    return tags;
  }
}
