import 'dart:developer';

import 'package:flutter/material.dart';

class FadeTransitionPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FadeTransitionPageState();
}

class FadeTransitionPageState extends State<FadeTransitionPage>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    //要和 SingleTickerProviderStateMixin 一起使用
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 2));
    _animation = Tween(begin: 0.0, end: 1.0).animate(_animationController)
      ..addStatusListener((status) {
        //可監聽動畫進度
        if (status == AnimationStatus.completed) log('動畫播完了');
      });
  }

  @override
  void dispose() {
    super.dispose();
    //離開這個頁面時，關閉動畫控制器。
    _animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(title: Text('FadeTransition')),
      body: FadeTransition(
        opacity: _animation,
        child: Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[FlutterLogo(size: 75), Text('歡迎使用')],
            )),
      ),
      floatingActionButton: FloatingActionButton.extended(
          icon: Icon(Icons.android),
          label: Text('開始動作'),
          onPressed: () {
            //啟動動畫
            _animationController.forward();
          }),
    );
  }
}
