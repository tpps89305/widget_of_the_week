import 'package:flutter/material.dart';

class SafeAreaPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SafeAreaPageState();
}

class SafeAreaPageState extends State<SafeAreaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text('SafeArea'),
      ),
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.all(8),
          children: List.generate(
              50,
              (i) => Text(
                    '${i + 1} 隻老廌打打打',
                    style: TextStyle(fontSize: 22),
                  )),
        ),
      ),
    );
  }
}
