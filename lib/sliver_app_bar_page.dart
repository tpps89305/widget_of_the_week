import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

class SliverAppBarPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SliverAppBarPageState();
}

class SliverAppBarPageState extends State<SliverAppBarPage> {
  @override
  Widget build(BuildContext context) {
    const SliverAppBar sliverAppBar = SliverAppBar(
      expandedHeight: 250,
      floating: true,
      flexibleSpace: FlexibleSpaceBar(
        title: Text("SliverAppBar"),
        background: Image(
          image: AssetImage('res/swimming.jpg'),
          fit: BoxFit.cover,
        ),
      ),
    );

    SliverToBoxAdapter sliverToBoxAdapter = SliverToBoxAdapter(
      // 讀取檔案是非同步程序，故使用 FutureBuilder 配合繪製畫面。
      child: FutureBuilder(
        future: _loadTextFile(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return Text(
                snapshot.data.toString(),
                style: TextStyle(fontSize: 18),
              );
            } else {
              return Text('讀取檔案時發生錯誤！');
            }
          } else {
            return Text('讀取檔案中⋯⋯');
          }
        },
      ),
    );

    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            // 一定要用 Sliver 系列的 Widget
            sliverAppBar,
            sliverToBoxAdapter
          ],
        ),
      ),
    );
  }

  /// 讀取檔案，這是非同步程序。
  ///
  /// 回傳的就是檔案裡的內容：一篇文章。
  Future _loadTextFile() async {
    return await rootBundle.loadString('res/test_text.txt');
  }
}
