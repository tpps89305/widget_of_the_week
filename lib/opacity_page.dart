import 'package:flutter/material.dart';

class OpacityPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => OpacityPageState();
}

class OpacityPageState extends State<OpacityPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(title: Text('Opacity')),
      body: Container(
        padding: EdgeInsets.all(8),
        color: Colors.amber,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                color: Colors.greenAccent,
                child: Align(
                  heightFactor: 1,
                  child: Text(
                    '普通的項目',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                padding: EdgeInsets.all(8),
              ),
            ),
            Expanded(
              flex: 2,
              child: Opacity(
                opacity: 0.5, //在這裡改變這個方塊的透明度
                child: Container(
                  color: Colors.redAccent,
                  child: Align(
                    heightFactor: 1,
                    child: Text(
                      '變成半透明的項目',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  padding: EdgeInsets.all(8),
                ),
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.blueAccent,
                child: Align(
                  heightFactor: 1,
                  child: Text(
                    '普通的項目',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                padding: EdgeInsets.all(8),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
