import 'package:flutter/material.dart';

class TablePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TablePageState();
}

class TablePageState extends State<TablePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(title: Text('Table')),
        body: Column(
          children: <Widget>[
            Text(
              '營業時間表',
              style: TextStyle(fontSize: 22),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: openTable(),
            ),
          ],
        ));
  }

  /// 製作營業時間表
  Table openTable() {
    return Table(
      border: TableBorder.all(color: Colors.grey), // 新增灰色格線
      columnWidths: {0: FractionColumnWidth(.2)}, // 加寬第 1 個欄位
      // 使每個儲存格內容垂直置中對齊
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: [
        TableRow(decoration: BoxDecoration(color: Colors.blueGrey), children: [
          _buildColumnHeaderText('時間＼星期'),
          _buildColumnHeaderText('一'),
          _buildColumnHeaderText('二'),
          _buildColumnHeaderText('三'),
          _buildColumnHeaderText('四'),
          _buildColumnHeaderText('五'),
          _buildColumnHeaderText('六')
        ]),
        TableRow(children: [
          _buildRowHeaderText('上午\n9:00-12:00'),
          _buildStatusIcon(false),
          _buildStatusIcon(false),
          _buildStatusIcon(false),
          _buildStatusIcon(false),
          _buildStatusIcon(false),
          _buildStatusIcon(true)
        ]),
        TableRow(children: [
          _buildRowHeaderText('下午\n2:00-5:30'),
          _buildStatusIcon(false),
          _buildStatusIcon(true),
          _buildStatusIcon(true),
          _buildStatusIcon(true),
          _buildStatusIcon(true),
          _buildStatusIcon(true)
        ]),
        TableRow(children: [
          _buildRowHeaderText('晚上\n6:00-9:30'),
          _buildStatusIcon(false),
          _buildStatusIcon(true),
          _buildStatusIcon(true),
          _buildStatusIcon(true),
          _buildStatusIcon(true),
          _buildStatusIcon(false)
        ]),
      ],
    );
  }

  /// 製作代表沒有營業狀態的圖示，[isThisTimeOpen] 為 true 代表有營業；反之為沒有營業。
  Icon _buildStatusIcon(bool isThisTimeOpen) {
    return Icon(
      isThisTimeOpen ? Icons.tag_faces : Icons.block,
      color: isThisTimeOpen ? Colors.black : Colors.black12,
    );
  }

  /// 製作列的標頭
  Text _buildRowHeaderText(String content) {
    return Text(
      content,
      textAlign: TextAlign.center,
    );
  }

  /// 製作欄位的標頭
  Text _buildColumnHeaderText(String content) {
    return Text(
      content,
      textAlign: TextAlign.center,
      style: TextStyle(color: Colors.white),
    );
  }
}
