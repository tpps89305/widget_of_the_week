import 'dart:developer';

import 'package:flutter/material.dart';

class FloatingActionButtonPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FloatingActionButtonPageState();
}

class FloatingActionButtonPageState extends State<FloatingActionButtonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(title: Text('FloatingActionButton')),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            log('按下了浮動按鈕');
          }),
      bottomNavigationBar: BottomAppBar( //TODO: 實作缺口形狀比較美觀
        color: Colors.greenAccent,
        child: Container(height: 50),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }
}
