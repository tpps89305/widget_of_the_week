import 'package:flutter/material.dart';

class ExpandedPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ExpandedPageState();
}

class ExpandedPageState extends State<ExpandedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(title: Text('Expanded')),
      body: SafeArea(
          child: Container(
        padding: EdgeInsets.all(8),
        color: Colors.amber,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                color: Colors.greenAccent,
                child: Align(
                  heightFactor: 1,
                  child: Text(
                    '普通的項目',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                padding: EdgeInsets.all(8),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                color: Colors.redAccent,
                child: Align(
                  heightFactor: 1,
                  child: Text(
                    '重要的項目',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                padding: EdgeInsets.all(8),
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.blueAccent,
                child: Align(
                  heightFactor: 1,
                  child: Text(
                    '普通的項目',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                padding: EdgeInsets.all(8),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
