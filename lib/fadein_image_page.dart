import 'package:flutter/material.dart';

class FadeInImagePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FadeInImagePageState();
}

class FadeInImagePageState extends State<FadeInImagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Fade In Image')),
      body: FadeInImage.assetNetwork(
          placeholder: 'res/swimming.jpg',
          image:
              'https://images.pexels.com/photos/1402787/pexels-photo-1402787.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'),
    );
  }
}
